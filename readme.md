WordPress Gulp
==============

WordPress Gulp is a Gulp Development environment for WordPress Development

## v0.0.4
One Gulp to rule them all!
* Now run Gulp from outside your projects folder.
* Gulp can be updated via repo and not affect your provate repo as it resides outside your project

## v0.0.3
Minor tweaks

## v0.0.2 Separate Gulp from Code
* You can now keep your Gulp folder in a separate directory! Suggested location is within your server root,
outside of client files.

## v0.0.1 Initial Release
* [Gulp](http://gulpjs.com) for compiling SASS to CSS, checking for JS errors, live reloading, concatenating and
minifying files
* Watch PHP files and reload
* Browser-Sync to remote server


## Installation

Before you start using gulp-wp make sure you have [NodeJS](http://nodejs.org) and [Gulp]
(http://gulpjs.com) installed. If you already have NodeJS installed then just run `npm install -g gulp`

Then:

* Clone the git repo - `git clone git@github.com:xingur/wordpress-gulp.git; cd gulp-wp` (use '&&' instead of ';' for
Linux/Mac)
* `npm install``
* `gulp`

## Thanks to:
[Andrew Taylor](http://www.ataylor.me/code/tutorials/using-gulp-js-in-wordpress-development/)
[Google Web-Starter-Kit](https://github.com/google/web-starter-kit)
## Contributing
Feel free to contribute!