// Set Variables
var gulp        = require('gulp'),
    $           = require('gulp-load-plugins')(),
    del         = require('del'),
    runSequence = require('run-sequence'),
    browserSync = require('browser-sync'),
    reload      = browserSync.reload;

// Set Paths
var projectName  = 'mySite.dev',
    themeName    = 'mySite',
    // Development Path
    dev          = '../' + projectName + '/dev/htdocs/',
    // Destination Path outside of Gulp directory
    dest         = '../' + projectName + '/dist/htdocs/content/themes/' + themeName + '/',
    // Asset Paths
    imgSrc       = dev + 'images/**/*.{png,jpg,jpeg,gif}',
    imgDest      = dest + 'images',
    cssSrc       = dev + '**/*.sass',
    cssDest      = dest,
    fontSrc      = dev + 'fonts/**/*',
    fontDest     = dest + 'fonts',
    jsMainSrc    = dev + 'js/main.js',
    jsVendorSrc  = dev + 'js/vendor/**/*.js',
    jsVendorDest = dest + 'js/vendor',
    jsDest       = dest + 'js',
    phpSrc       = dev + '**/*.php', // '../example.dev/**/.*.php
    phpDest      = dest

// Local URL
var localURL = projectName;

//SASS Production
gulp.task('sass', function () {
    return gulp.src(cssSrc)
        .pipe($.plumber())
        .pipe($.rubySass({
            style: 'compressed',
            precision: '10'
        }))
        .pipe($.autoprefixer())
        .pipe(gulp.dest(cssDest))
        .pipe($.combineMediaQueries())
        .pipe($.minifyCss({keepSpecialComments: 1}))
        .pipe(gulp.dest(cssDest))
        .pipe($.size({title: 'Sass Production'}));
});

// Sass Development
gulp.task('sassDev', function () {
    return gulp.src(cssSrc)
        .pipe($.plumber())
        .pipe($.rubySass({
            style: 'nested',
            precision: '3'
        }))
        .pipe(gulp.dest(cssDest))
        .pipe($.size({title: 'Sass Development'}));
});

//Images
gulp.task('images', function () {
    return gulp.src(imgSrc)
        //use cache to only target new/changed files
        .pipe($.imagemin({
            progressive: true,
            interlaced: true
        }))
        .pipe(gulp.dest(imgDest))
        .pipe($.size({title: 'Images'}));
});

//Images Build
gulp.task('buildImages', function () {
    return gulp.src(imgSrc)
        //don't use cache on build task since we are rebuilding all images
        .pipe($.imagemin({
            progressive: true,
            interlaced: true
        }))
        //save optimized image files
        .pipe(gulp.dest(imgDest))
        .pipe($.size({title: 'Rebuilt Images'}));
});

//Fonts
gulp.task('fonts', function () {
    return gulp.src(fontSrc)
        //don't do anything to fonts, just save 'em
        .pipe(gulp.dest(fontDest))
        .pipe($.size({title: 'fonts'}));
});

//Save Vendor Scripts (enqueue with WP)
gulp.task('vendorScripts', function () {
    return gulp.src(jsVendorSrc)
        .pipe(gulp.dest(jsVendorDest))
        .pipe($.size({title: 'Vendor Scripts'}));
});

//Main Script
gulp.task('mainScript', function () {
    return gulp.src(jsMainSrc)
        .pipe($.uglify())
        .pipe(gulp.dest(jsDest))
        .pipe($.size({title: 'Main Script'}));
});

//All Scripts
gulp.task('allScripts', function (cb) {
    runSequence('vendorScripts', 'mainScript', cb);
});

// Clean Output Directory
// use force option to clean outside of gulp directory
gulp.task('buildClean', del.bind(null, [dest], {force: true}));

// Copy PHP files to dest
gulp.task('php', function () {
    gulp.src(phpSrc)
        .pipe(gulp.dest(phpDest))
        .pipe($.size({title: 'PHP'}));
})


//BrowserSync
gulp.task('browserSync', function () {
    //declare files to watch
    //look for final files in build directory
    var files = [
        jsDest + '**/*.js',
        cssDest + '**/*.css',
        imgDest + '**/*.{png,jpg,jpeg,gif}',
        phpDest + '**/*.php'
    ];
    browserSync.init(files, {
        proxy: localURL
    })
});

// Watch
gulp.task('watch', function () {
    gulp.watch(phpSrc, ['php', reload]);
    gulp.watch(jsMainSrc, ['mainScript', reload]);
    gulp.watch(cssSrc, ['sassDev']);
    gulp.watch(imgSrc, ['images', reload]);

});

// Default task
gulp.task('default', ['buildClean'], function (cb) {
    runSequence('sassDev', ['images','fonts','allScripts','php', 'browserSync','watch'], cb);
});

//Build Task
gulp.task('build', function (cb) {
    runSequence('buildClean', ['buildImages', 'fonts', 'php', 'allScripts', 'sass'], cb);
});
